const unified = require('unified');
const markdown = require('remark-parse');
const visit = require('unist-util-visit');

function parseReferences(references) {
  const reports = [];
  const tree = unified().use(markdown).parse(references);
  visit(tree, 'link', ({ url }) => {
    reports.push({ url });
  });
  return reports;
}

module.exports = function parseAuditAdvisory(advisory) {
  let identifiers = advisory.cves
      .map((cve) => ({
        name: cve,
        url: `https://nvd.nist.gov/vuln/detail/${cve}`,
      }));

  advisory.cwe.forEach((cwe) => {
    identifiers = identifiers
        .concat({
          name: cwe,
          url: `https://cwe.mitre.org/data/definitions/${cwe.replace(/^CWE-/, '')}`,
        })
  })

  identifiers.sort((a, b) => (a.name < b.name ? -1 : 1));

  return {
    confidence: 'High',
    description: advisory.overview,
    identifiers,
    instances: []
        .concat(...advisory.findings.map(({ paths }) => paths.map((method) => ({ method }))))
        .sort(),
    links: [{ url: advisory.url }]
        .concat(parseReferences(advisory.references))
        .sort((a, b) => (a.url < b.url ? -1 : 1)),
    location: {
      file: 'yarn.lock',
    },
    message: advisory.title,
    namespace: advisory.module_name,
    severity: Array.from(advisory.severity, (c, i) => (i ? c : c.toUpperCase())).join(''),
    solution: advisory.recommendation,
  };
};
