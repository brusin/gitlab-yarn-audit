#!/usr/bin/env node
const { spawn } = require('child_process');

const { filterSync, mapSync, parse, split, writeArray } = require('event-stream');
const { write } = require('gitlab-artifact-report');

const filterDuplicates = require('./filterDuplicates');
const parseAuditAdvisory = require('./parseAuditAdvisory');

const yarn = spawn('yarn', ['audit', '--json']);

yarn.stdout
  .pipe(split())
  .pipe(parse())
  .pipe(filterSync(({ type }) => type === 'auditAdvisory'))
  .pipe(mapSync(({ data }) => data.advisory))
  .pipe(filterSync(filterDuplicates()))
  .pipe(mapSync(parseAuditAdvisory))
  .pipe(
    writeArray((err, array) => {
      write('dependency_scanning', array);
    }),
  )
  .on('close', (code) => {
    process.exit(code);
  })
  .on('error', (err) => {
    // eslint-disable-next-line no-console
    console.error(err);
    process.exit(1);
  });
