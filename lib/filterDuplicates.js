module.exports = function filterDuplicates() {
  const ids = new Set();

  return ({ id }) => {
    if (ids.has(id)) {
      return false;
    }
    ids.add(id);
    return true;
  };
};
