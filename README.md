# GitLab Yarn Audit

> Expose `yarn audit` results as GitLab dependency scanning artifacts

## Installation

```sh
yarn add -WD gitlab-yarn-audit
```

## Usage

Configure the following GitLab job.

```yaml
dependency scanning:
  image: node:10-alpine
  allow_failure: true
  script:
    - yarn --frozen-lockfile
    - yarn gitlab-yarn-audit
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning.json
```

Of course the image to use should be adjusted to your needs. This will automagically run
`yarn audit` under the hood, and show the results in the GitLab dependency scanning section of the
security report.

The tool will exit with the status code of `yarn audit`. This will be non-zero when a vulnerable
dependency is found. Because new vulnerabilities may be reported at any moment, is is recommended to
set the `allow_failure` option to `true`. This prevents pipelines for unrelated changes from
failing.
